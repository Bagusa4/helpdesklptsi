<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/debug', function () {
    return App\Models\Tiket::class;
});

/**
 * Static Pages
 */
Route::group(['prefix' => 'page'], function () {
    Route::get('dokumentasi', 'PageController@dokumentasi');
    Route::get('formulir', 'PageController@formulir');
});

/**
 * All User Authentications
 */
Auth::routes();
Route::get('register/success', 'SuccessController@register')->name('user.register.success');
Route::group(['prefix' => 'user'], function() {
    Route::group(['namespace' => 'Manage'], function () {
        Route::get('register', 'UserRegisterController@showRegistrationForm')->name('manage.user.register');
        Route::post('register', 'UserRegisterController@register');
    });

    Route::group(['namespace' => 'Profile'], function () {
        Route::get('profile', 'UserController@index')->name('user.profile.index');
        Route::get('profile/edit', 'UserController@edit')->name('user.profile.edit');
        Route::post('profile/edit', 'UserController@update')->name('user.profile.update');
        Route::get('profile/edit/password', 'UserController@showChangePasswordForm')->name('user.profile.edit.password');
        Route::post('profile/edit/password', 'UserController@changePassword');
    });
});

Route::get('/home', 'HomeController@index');

Route::group(['namespace' => 'Manage'], function () {
    Route::group(['prefix' => '/manage'], function() {
        Route::get('admin/index/data', 'AdminController@data')->name('manage.admin.index.data');
        Route::get('admin/index/options', 'AdminController@options');
        Route::resource('admin', 'AdminController', [
            'names' => [
                'index' => 'manage.admin.index',
                'store' => 'manage.admin.store',
                'create' => 'manage.admin.create',
                'show' => 'manage.admin.show',
                'destroy' => 'manage.admin.destroy',
                'update' => 'manage.admin.update',
                'edit' => 'manage.admin.edit',
            ],
            'except' => [
                'create', 'show', 'edit', 'store'
            ],
            'parameters' => [
                'admin' => 'user'
            ]
        ]);

        Route::get('allusers/index/data', 'AllUsersController@data')->name('manage.allusers.index.data');
        Route::get('allusers/index/options', 'AllUsersController@options');
        Route::resource('allusers', 'AllUsersController', [
            'names' => [
                'index' => 'manage.allusers.index',
                'store' => 'manage.allusers.store',
                'create' => 'manage.sllusers.create',
                'show' => 'manage.allusers.show',
                'destroy' => 'manage.allusers.destroy',
                'update' => 'manage.allusers.update',
                'edit' => 'manage.allusers.edit',
            ],
            'except' => [
                'create', 'show', 'edit', 'store'
            ],
            'parameters' => [
                'allusers' => 'user'
            ]
        ]);

        Route::get('operator/index/data', 'OperatorController@data')->name('manage.operator.index.data');
        Route::get('operator/index/options', 'OperatorController@options');
        Route::resource('operator', 'OperatorController', [
            'names' => [
                'index' => 'manage.operator.index',
                'store' => 'manage.operator.store',
                'create' => 'manage.operator.create',
                'show' => 'manage.operator.show',
                'destroy' => 'manage.operator.destroy',
                'update' => 'manage.operator.update',
                'edit' => 'manage.operator.edit',
            ],
            'except' => [
                'create', 'show', 'edit', 'store'
            ],
            'parameters' => [
                'operator' => 'user'
            ]
        ]);

        Route::get('teknisi/index/data', 'TeknisiController@data')->name('manage.teknisi.index.data');
        Route::get('teknisi/index/options', 'TeknisiController@options');
        Route::resource('teknisi', 'TeknisiController', [
            'names' => [
                'index' => 'manage.teknisi.index',
                'store' => 'manage.teknisi.store',
                'create' => 'manage.teknisi.create',
                'show' => 'manage.teknisi.show',
                'destroy' => 'manage.teknisi.destroy',
                'update' => 'manage.teknisi.update',
                'edit' => 'manage.teknisi.edit',
            ],
            'except' => [
                'create', 'show', 'edit', 'store'
            ],
            'parameters' => [
                'teknisi' => 'user'
            ]
        ]);

        Route::get('user/index/data', 'UserController@data')->name('manage.user.index.data');
        Route::get('user/index/options', 'UserController@options');
        Route::resource('user', 'UserController', [
            'names' => [
                'index' => 'manage.user.index',
                'store' => 'manage.user.store',
                'create' => 'manage.user.create',
                'show' => 'manage.user.show',
                'destroy' => 'manage.user.destroy',
                'update' => 'manage.user.update',
                'edit' => 'manage.user.edit',
            ],
            'except' => [
                'create', 'show', 'edit', 'store'
            ],
            'parameters' => [
                'user' => 'user'
            ]
        ]);

        /**
         * Unit Kerja / Fakultas route
         */
        Route::get('unitkerja/index/data', 'UnitKerjaController@data');
        Route::resource('unitkerja', 'UnitKerjaController', [
            'names' => [
                'index' => 'manage.unitkerja.index',
                'store' => 'manage.unitkerja.store',
                'create' => 'manage.unitkerja.create',
                'show' => 'manage.unitkerja.show',
                'destroy' => 'manage.unitkerja.destroy',
                'update' => 'manage.unitkerja.update',
                'edit' => 'manage.unitkerja.edit',
            ],
            'except' => [
                'create', 'show', 'edit'
            ],
            'parameters' => [
                'unitkerja' => 'unitKerja'
            ]
        ]);

        /**
         * Confirmations route
         */
        Route::group(['prefix' => 'confirmation', 'namespace' => 'Confirmation'], function () {
            Route::get('users/index/data', 'UsersController@data');
            Route::resource('users', 'UsersController', [
                'names' => [
                    'index' => 'manage.confirmation.users.index',
                    'store' => 'manage.confirmation.users.store',
                    'create' => 'manage.confirmation.users.create',
                    'show' => 'manage.confirmation.users.show',
                    'destroy' => 'manage.confirmation.users.destroy',
                    'update' => 'manage.confirmation.users.update',
                    'edit' => 'manage.confirmation.users.edit',
                ],
                'only' => [
                    'index', 'update', 'destroy'
                ]
            ]);
        });

        /**
         * Trashed route
         */
        Route::group(['prefix' => 'trashed', 'namespace' => 'Trashed'], function () {
            Route::get('users/index/data', 'UsersController@data');
            Route::put('users/{id}/restore', 'UsersController@restore');
            Route::delete('users/{id}/delete', 'UsersController@delete');
            Route::resource('users', 'UsersController', [
                'names' => [
                    'index' => 'manage.trashed.users.index',
                    'store' => 'manage.trashed.users.store',
                    'create' => 'manage.trashed.users.create',
                    'show' => 'manage.trashed.users.show',
                    'destroy' => 'manage.trashed.users.destroy',
                    'update' => 'manage.trashed.users.update',
                    'edit' => 'manage.trashed.users.edit',
                ],
                'only' => [
                    'index'
                ]
            ]);
        });

        /**
         * Managemen Tiket
         */
        Route::group(['namespace' => 'Tiket'], function () {
            Route::get('tiket/index/data', 'TiketController@data')->name('manage.tiket.index.data');
            Route::get('tiket/index/find/{tiket}', 'TiketController@find')->name('manage.tiket.index.find');
            Route::get('tiket/index/options', 'TiketController@options')->name('manage.tiket.index.options');
            Route::get('tiket/{tiket}/pdf', 'TiketController@pdf')->name('manage.tiket.pdf');
            Route::resource('tiket', 'TiketController', [
                'names' => [
                    'index' => 'manage.tiket.index',
                    'store' => 'manage.tiket.store',
                    'create' => 'manage.tiket.create',
                    'show' => 'manage.tiket.show',
                    'destroy' => 'manage.tiket.destroy',
                    'update' => 'manage.tiket.update',
                    'edit' => 'manage.tiket.edit',
                ],
                'except' => [
                    'create', 'edit', 'store'
                ]
            ]);

            Route::get('kategori/index/data', 'KategoriTiketController@data')->name('manage.tiket.kategori.index.data');
            Route::get('kategori/index/search/{data}', 'KategoriTiketController@search')->name('manage.tiket.kategori.index.search');
            Route::resource('kategori', 'KategoriTiketController', [
                'names' => [
                    'index' => 'manage.tiket.kategori.index',
                    'store' => 'manage.tiket.kategori.store',
                    'create' => 'manage.tiket.kategori.create',
                    'show' => 'manage.tiket.kategori.show',
                    'destroy' => 'manage.tiket.kategori.destroy',
                    'update' => 'manage.tiket.kategori.update',
                    'edit' => 'manage.tiket.kategori.edit',
                ],
                'except' => [
                    'create', 'show', 'edit'
                ]
            ]);

            Route::get('tiket/{tiket}/info/index/data', 'TiketInfoController@data');
            Route::resource('tiket.info', 'TiketInfoController', [
                'names' => [
                    'index' => 'manage.tiket.info.index',
                    'store' => 'manage.tiket.info.store',
                    'create' => 'manage.tiket.info.create',
                    'show' => 'manage.tiket.info.show',
                    'destroy' => 'manage.tiket.info.destroy',
                    'update' => 'manage.tiket.info.update',
                    'edit' => 'manage.tiket.info.edit',
                ],
                'except' => [
                    'index', 'create', 'show', 'edit'
                ]
            ]);

            Route::get('rekap/index/data', 'RekapTiketController@data');
            Route::get('rekap/index/options', 'RekapTiketController@options');
            Route::get('rekap/pdf', 'RekapTiketController@pdf');
            Route::resource('rekap', 'RekapTiketController', [
                'names' => [
                    'index' => 'manage.tiket.rekap.index',
                    'store' => 'manage.tiket.rekap.store',
                    'create' => 'manage.tiket.rekap.create',
                    'show' => 'manage.tiket.rekap.show',
                    'destroy' => 'manage.tiket.rekap.destroy',
                    'update' => 'manage.tiket.rekap.update',
                    'edit' => 'manage.tiket.rekap.edit',
                ],
                'only' => [
                    'index', 'show'
                ]
            ]);
        });
    });

    Route::resource('/manage', 'ManageController', [
        'names' => [
            'index' => 'manage.index',
            'store' => 'manage.store',
            'create' => 'manage.create',
            'show' => 'manage.show',
            'destroy' => 'manage.destroy',
            'update' => 'manage.update',
            'edit' => 'manage.edit',
        ],
        'only' => [
            'index'
        ]
    ]);
});


Route::group(['namespace' => 'Tiket'], function () {
    Route::group(['prefix' => '/tiket'], function () {
        Route::group(['middleware' => 'auth'], function () {
            Route::get('/ku/index/data', 'TiketKuController@data');
            Route::get('/ku/index/find/{tiket}', 'TiketKuController@find');
            Route::get('/ku/index/options', 'TiketKuController@options');
            Route::resource('/ku', 'TiketKuController', [
                'names' => [
                    'index' => 'tiket.ku.index',
                    'store' => 'tiket.ku.store',
                    'create' => 'tiket.ku.create',
                    'show' => 'tiket.ku.show',
                    'destroy' => 'tiket.ku.destroy',
                    'update' => 'tiket.ku.update',
                    'edit' => 'tiket.ku.edit',
                ],
                'only' => [
                    'index', 'show', 'store', 'create'
                ],
                'parameters' => [
                    'ku' => 'tiket'
                ]
            ]);
        });
    });

    Route::get('/tiket/index/data', 'TiketController@data');
    Route::get('/tiket/index/find/{tiket}', 'TiketController@find');
    Route::resource('/tiket', 'TiketController', [
        'names' => [
            'index' => 'tiket.index',
            'store' => 'tiket.store',
            'create' => 'tiket.create',
            'show' => 'tiket.show',
            'destroy' => 'tiket.destroy',
            'update' => 'tiket.update',
            'edit' => 'tiket.edit',
        ],
        'only' => [
            'index', 'show'
        ]
    ]);
});

Route::group(['prefix' => 'teknisi', 'middleware' => 'role:Teknisi', 'namespace' => 'Teknisi'], function() {
    Route::get('tiket/index/data', 'TiketController@data');
    Route::get('tiket/index/find/{tiket}', 'TiketController@find');
    Route::resource('tiket', 'TiketController', [
        'names' => [
            'index' => 'teknisi.tiket.index',
            'store' => 'teknisi.tiket.store',
            'create' => 'teknisi.tiket.create',
            'show' => 'teknisi.tiket.show',
            'destroy' => 'teknisi.tiket.destroy',
            'update' => 'teknisi.tiket.update',
            'edit' => 'teknisi.tiket.edit',
        ],
        'only' => [
            'index', 'show'
        ]
    ]);

    Route::get('tiket/{tiket}/info/index/data', 'TiketInfoController@data');
    Route::resource('tiket.info', 'TiketInfoController', [
        'names' => [
            'index' => 'teknisi.tiket.info.index',
            'store' => 'teknisi.tiket.info.store',
            'create' => 'teknisi.tiket.info.create',
            'show' => 'teknisi.tiket.info.show',
            'destroy' => 'teknisi.tiket.info.destroy',
            'update' => 'teknisi.tiket.info.update',
            'edit' => 'teknisi.tiket.info.edit',
        ],
        'only' => [
            'store', 'update', 'destroy'
        ]
    ]);
});
