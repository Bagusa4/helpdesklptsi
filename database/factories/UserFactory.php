<?php
/**
 * Created by PhpStorm.
 * User: Smecon - Smecon
 * Date: 3/6/2017
 * Time: 1:30 PM
 */

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'active' => $faker->numberBetween(0, 1),
        'unit_kerja_id' => $faker->numberBetween(1, 4)
    ];
});

$factory->define(App\Models\IdentitasUsers::class, function (Faker\Generator $faker) {
     return [
         'tanggal_lahir' => $faker->date,
         'tempat_lahir' => $faker->city,
         'alamat' => $faker->address,
         'jabatan' => $faker->jobTitle
     ];
});