@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('alias.profile.view.detail')</div>
                    <div class="panel-body">
                       <table class="table table-striped">
                           <tbody>
                            <tr>
                                <td>@lang('form.user.id.title')</td>
                                <td>:</td>
                                <td>{{ $user->id }}</td>
                            </tr>
                            <tr>
                                <td>@lang('form.user.username.title')</td>
                                <td>:</td>
                                <td>{{ $user->name }}</td>
                            </tr>
                            <tr>
                                <td>@lang('form.user.email.title')</td>
                                <td>:</td>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td>@lang('form.user.created_at.title')</td>
                                <td>:</td>
                                <td>{{ $user->created_at }}</td>
                            </tr>
                            <tr>
                                <td>@lang('form.user.full_name.title')</td>
                                <td>:</td>
                                <td>{{ $user->identitas->nama_lengkap }}</td>
                            </tr><tr>
                                <td>@lang('form.user.birth_date.title')</td>
                                <td>:</td>
                                <td>{{ $user->identitas->tanggal_lahir }}</td>
                            </tr>
                            <tr>
                                <td>@lang('form.user.birth_place.title')</td>
                                <td>:</td>
                                <td>{{ $user->identitas->tempat_lahir }}</td>
                            </tr>
                            <tr>
                                <td>@lang('form.user.address.title')</td>
                                <td>:</td>
                                <td>{{ $user->identitas->alamat }}</td>
                            </tr>
                            <tr>
                                <td>@lang('form.user.status.title')</td>
                                <td>:</td>
                                <td>{{ $user->identitas->status }}</td>
                            </tr>
                            <tr>
                                <td>@lang('form.user.chair.title')</td>
                                <td>:</td>
                                <td>{{ $user->identitas->jabatan }}</td>
                            </tr>
                            <tr>
                                <td>@lang('form.user.work_unit.title')</td>
                                <td>:</td>
                                <td>{{ $user->unitKerja->nama }} - {{ $user->unitKerja->deskripsi }}</td>
                            </tr>
                           </tbody>
                       </table>
                    </div>
                    <div class="panel-footer">
                        <a type="button" class="btn btn-info" href="{{ url('/user/profile/edit') }}">@lang('button.confirmation.update')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
