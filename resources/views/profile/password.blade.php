@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('alias.profile.password.detail')</div>
                    <div class="panel-body">
                        <form id="edit-user-password" class="form-horizontal" role="form" method="post"
                              action="{{ url('/user/profile/edit/password') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">@lang('form.user.password.title')</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">@lang('form.user.password.confirmation.title')</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-info"
                                onclick="event.preventDefault();
                                document.getElementById('edit-user-password').submit();">@lang('button.confirmation.save')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection