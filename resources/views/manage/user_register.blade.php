@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('alias.menu.register')</div>
                <div class="panel-body">
                    <form id="admin-user-registration" class="form-horizontal" role="form" method="POST" action="{{ url('/user/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">@lang('form.user.username.title')</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">@lang('form.user.email.title')</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">@lang('form.user.password.title')</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">@lang('form.user.password.confirmation.title')</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('unit_kerja') ? ' has-error' : '' }}">
                            <label for="unit-kerja" class="col-md-4 control-label">@lang('form.user.work_unit.title')</label>

                            <div class="col-md-6">
                                <select name="unit_kerja" class="form-control" required>
                                    <option value="null">@lang('form.user.work_unit.select')</option>
                                    @foreach($data['unit'] as $unit)
                                        <option value="{{  $unit['id'] }}">{{ $unit['nama'] }} - {{ $unit['deskripsi'] }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('unit_kerja'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('unit_kerja') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('role') ? ' has-error' : '' }}">
                            <label for="selectRole" class="col-md-4 control-label">@lang('form.user.role.title')</label>

                            <div class="col-md-6">
                                <select name="role" class="form-control" form="admin-user-registration" required>
                                    <option value="null">@lang('form.user.role.select')</option>
                                    @foreach($data['role'] as $role)
                                        <option value="{{ $role['id'] }}">{{ $role['display_name'] }} - {{ $role['description'] }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('role'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="active" class="col-md-4 control-label">@lang('form.user.active.detail')</label>

                            <div class="col-md-6">
                                <select name="active" class="form-control" form="admin-user-registration">
                                    <option value="true" selected>@lang('button.confirmation.yes')</option>
                                    <option value="false">@lang('button.confirmation.no')</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang('button.create.user')
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
