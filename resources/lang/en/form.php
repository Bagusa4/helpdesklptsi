<?php
/**
 * Created by PhpStorm.
 * User: Bagusa4 - Smecon
 * Date: 4/2/2017
 * Time: 7:08 PM
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Nama dari form field
    |--------------------------------------------------------------------------
    |
    | Nama nama field pada form
    |
    */

    'filter' => [
        'filter' => 'Filters',
        'by_id' => 'Cari berdasarkan ID',
        'search' => 'Cari data',
        'order_by' => [
            'order_by' => 'Urutkan berdasarkan . . .',
            'asc' => 'Urutkan berdasarkan dari yang terlama ke yang terbaru - ASC',
            'desc' => 'Urutkan berdesarkan dari yang terbaru ke yang terlama - DESC'
        ]
    ],
    'tiket' => [
        'create' => [
            'nama_pelapor' => 'Nama Pelapor',
            'judul' => 'Nama/Judul Tiket/Laporan',
            'deskripsi' => 'Deskripsi Tiket/Laporan',
            'kategori' => 'Kategori Tiket/Laporan',
            'unit_kerja' => [
                'unit_kerja' => 'Unit Kerja / Fakultas',
                'select' => 'Pilih Unit Kerja / Fakultas'
            ]
        ],
        'show' => [
            'info' => [
                'status' => [
                    'status' => 'Status Info',
                    'detail' => 'Masukkan detail info'
                ],
                'deskripsi' => [
                    'deskripsi' => 'Deskripsi Info',
                    'detail' => 'Masukkan detail deskripsi info'
                ]
            ]
        ]
    ],
    'title' => [
        'name' => 'Nama',
    ],
    'detail' => [
        'name' => 'Masukan Nama',
    ]
];