<?php
/**
 * Created by PhpStorm.
 * User: Bagusa4 - Smecon
 * Date: 3/27/2017
 * Time: 12:12 PM
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Pesan Umum Yang digunakan dalam aplikasi ini
    |--------------------------------------------------------------------------
    |
    | Pesan - pesan umum yang digunakan di aplikasi ini
    |
    */

    'welcome' => "Untuk melaporkan kerusakan jaringan komputer di Universitas Jenderal Soedirman klik Login terlebih dahulu :)",
    'dashboard' => "Dashboard - You are logged in! - :user",

    'success' => [
        'confirmation' => [
            'user' => [
                'accept' => [
                    'title' => "Pengguna Berhasil di Konfirmasi !!!",
                    'detail' => "",
                ],
                'decline' => [
                    'title' => "Pengguna Berhasil di Tolak dan Hapus !!!",
                    'detail' => ""
                ],
            ]
        ],
        'trashed' => [
            'user' => [
                'restore' => [
                    'title' => "Pengguna Berhasil di Kembalikan !!!",
                    'detail' => "",
                ],
                'delete' => [
                    'title' => "Pengguna Berhasil di Hapus Permanen !!!",
                    'detail' => ""
                ],
            ]
        ],
        'unit_kerja' => [
            'create' => [
                'title' => 'Berhasil Menambahkan Unit Kerja / Fakultas',
                'detail' => ''
            ],
            'update' => [
                'title' => 'Berhasil Mengubah Unit Kerja / Fakultas',
                'detail' => ''
            ],
            'delete' => [
                'title' => 'Berhasil Menghapus Unit Kerja / Fakultas',
                'detail' => ''
            ],
        ],
        'user' => [
            'register' => [
                'title' => 'Selamat Anda Berhasil Mendaftar !!!',
                'detail' => 'Mengunggu persetujuan admin terlebih dahulu :)'
            ],
            'update' => [
                'title' => 'Pengguna Berhasil di Ubah !!!',
                'detail' => ''
            ],
            'delete' => [
                'title' => 'Pengguna Berhasil di Hapus !!!',
                'detail' => ''
            ]
        ],
        'tiket' => [
            'add' => [
                'info' => [
                    'title' => 'Info Berhasil di Tambahkan !!!',
                    'detail' => ''
                ]
            ],
            'delete' => [
                'info' => [
                    'title' => "Info Berhasil di Hapus !!!",
                    'detail' => ''
                ]
            ],
            'update' => [
                'info' => [
                    'title' => "Info Berhasil di Ubah !!!",
                    'detail' => ''
                ],
                'title' => 'Laporan/Keluhan ID :tiket',
                'detail' => 'Tiket berhasil di update'
            ],
            'create' => [
                'title' => 'Sukses',
                'detail' => '',
                'redirect' => [
                    'laporanku' => 'Klik disini untuk melihat laporan',
                ],
            ],
        ],
    ],

    'failed' => [
        'confirmation' => [
            'user' => [
                'accept' => [
                    'title' => "Pengguna Gagal di Konfirmasi !!!",
                    'detail' => "",
                ],
                'decline' => [
                    'title' => "Pengguna Gagal di Tolak dan Hapus !!!",
                    'detail' => ""
                ],
            ]
        ],
        'trashed' => [
            'user' => [
                'restore' => [
                    'title' => "Pengguna Gagal di Kembalikan !!!",
                    'detail' => "",
                ],
                'delete' => [
                    'title' => "Pengguna Gagal di Hapus Permanen !!!",
                    'detail' => ""
                ],
            ]
        ],
        'unit_kerja' => [
            'create' => [
                'title' => 'Gagal Menambahkan Unit Kerja / Fakultas',
                'detail' => ''
            ],
            'update' => [
                'title' => 'Gagal Mengubah Unit Kerja / Fakultas',
                'detail' => ''
            ],
            'delete' => [
                'title' => 'Gagal Menghapus Unit Kerja / Fakultas',
                'detail' => ''
            ],
        ],
        'user' => [
            'update' => [
                'title' => "Pengguna Gagal di Ubah !!!",
                'detail' => ""
            ],
            'delete' => [
                'title' => "Pengguna Gagal di Hapus !!!",
                'detail' => ""
            ]
        ],
        'tiket' => [
            'add' => [
                'info' => [
                    'title' => 'Info Gagal di Tambahkan !!!',
                    'detail' => ''
                ]
            ],
            'delete' => [
                'info' => [
                    'title' => "Info Gagal di Hapus !!!",
                    'detail' => ''
                ]
            ],
            'update' => [
                'info' => [
                    'title' => "Info Gagal di Ubah !!!",
                    'detail' => ''
                ],
            ]
        ]
    ],

    'pending' => [
        'title' => 'Pending',
        'detail' => 'Klik Simpan Untuk Menyimpan Perubahan'
    ],

    'modal' => [
        'unit_kerja' => [
            'delete' => [
                'title' => 'Apakah anda ingin menghapus Unit Kerja / Fakultas ( :nama - :deskripsi ) ???',
                'detail' => ''
            ]
        ],
        'user' => [
            'update' => [
                'title' => '',
                'detail' => ''
            ],
            'delete' => [
                'title' => 'Apakah anda ingin menghapus ( :username - :full_name ) ???',
                'detail' => ''
            ],
            'confirmation' => [
                'decline' => [
                    'title' => 'Menolak user ( :id - :name ) juga akan menghapus user tersebut dalam database. Yakin ???',
                    'detail' => ''
                ]
            ],
            'trashed' => [
                'delete' => [
                    'title' => 'Menghapus user ( :id - :name ) akan mempengaruhi data pada laporan / keluhan yang telah di buat oleh user yang bersangkutan !!!.
                    Aksi ini tidak dapat di batalkan, dan user akan di hapus secara permanen pada database !!!',
                    'detail' => '',
                ]
            ]
        ],
        'tiket' => [
            'show' => [
                'info' => [
                    'delete' => 'Apakah anda akan menghapus info :info ???'
                ],
                'delete' => 'Yakin mau hapus Tiket Id :tiketId ???'
            ],
            'kategori' => [
                'delete' => 'Apakah anda ingin menghapus kategori :kategori ???'
            ]
        ],
    ]
];