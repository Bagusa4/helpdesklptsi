<?php
/**
 * Created by PhpStorm.
 * User: Bagusa4 - Smecon
 * Date: 4/2/2017
 * Time: 7:08 PM
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Nama dari form field
    |--------------------------------------------------------------------------
    |
    | Nama nama field pada form
    |
    */

    'user' => [
        'id' => [
            'title' => 'ID User',
            'detail' => 'Penomoran Otomatis'
        ],
        'username' => [
            'title' => 'Nama Pengguna / NIM',
            'detail' => 'Masukan Nama Pengguna / NIM'
        ],
        'email' => [
            'title' => 'Alamat E-Mail',
            'detail' => 'Masukan alamat E-Mail'
        ],
        'password' => [
            'title' => 'Kata Sandi',
            'detail' => 'Masukan Kata Sandi',
            'confirmation' => [
                'title' => 'Konfirmasi Kata Sandi',
                'detail' => ''
            ]
        ],
        'full_name' => [
            'title' => 'Nama Lengkap',
            'detail' => 'Masukan Nama Lengkap'
        ],
        'birth_date' => [
            'title' => 'Tanggal Lahir',
            'detail' => 'Masukan Tanggal Lahir'
        ],
        'birth_place' => [
            'title' => 'Tempat Lahir',
            'detail' => 'Masukan Tempat Lahir'
        ],
        'address' => [
            'title' => 'Alamat / Tempat Tinggal',
            'detail' => 'Masukan Alamat'
        ],
        'status' => [
            'title' => 'Status',
            'detail' => 'Masukan Status'
        ],
        'chair' => [
            'title' => 'Jabatan',
            'detail' => 'Masukan Jabatan'
        ],
        'work_unit' => [
            'title' => 'Unit Kerja / Fakultas',
            'detail' => 'Masukkan Unit Kerja / Fakultas',
            'select' => 'Pilih Unit Kerja / Fakultas'
        ],
        'active' => [
            'title' => 'Aktif',
            'detail' => 'Centang Ke Aktifan User'
        ],
        'role' => [
            'title' => 'Hak Akses',
            'detail' => 'Pilih Hak Akses',
            'select' => 'Pilih salah satu Hak Akses'
        ],
        'created_at' => [
            'title' => 'Tanggal Pembuatan',
            'detail' => ''
        ],
        'updated_at' => [
            'title' => 'Tanggal Perubahan',
            'detail' => ''
        ],
        'deleted_at' => [
            'title' => 'Tanggal Penghapusan',
            'detail' => ''
        ]
    ],

    'unit_kerja' => [
        'nama' => [
            'title' => 'Judul Unit Kerja / Fakultas',
            'detail' => 'Masukkan Judul Unit Kerja / Fakultas'
        ],
        'deskripsi' => [
            'title' => 'Deskripsi Unit Kerja / Fakultas',
            'detail' => 'Masukkan Deskripsi Unit Kerja / Fakultas'
        ]
    ],

    'filter' => [
        'filter' => 'Filters',
        'by_id' => 'Cari berdasarkan ID',
        'search' => 'Cari data',
        'date' => [
            'title' => 'Tanggal',
            'detail' => 'Cari Berdasarkan tanggal'
        ],
        'month' => [
            'title' => 'Bulan',
            'detail' => 'Cari berdasarkan bulan'
        ],
        'order_by' => [
            'order_by' => 'Pengurutan',
            'detail' => 'Urutkan berdasarkan . . .',
            'asc' => 'Urutkan berdasarkan dari yang terlama ke yang terbaru - ASC',
            'desc' => 'Urutkan berdesarkan dari yang terbaru ke yang terlama - DESC'
        ],
        'role' => [
            'title' => 'Hak Akses',
            'detail' => 'Pilih Berdasarkan Hak Akses'
        ],
        'work_unit' => [
            'title' => 'Unit Kerja / Fakultas',
            'detail' => 'Pilih Berdasarkan Unit Kerja / Fakultas'
        ]
    ],

    'tiket' => [
        'id' => 'ID Laporan',
        'nama_pelapor' => [
            'title' => 'Nama Pelapor',
            'detail' => 'Masukkan Nama Pelapor'
        ],
        'judul' => [
            'title' => 'Nama/Judul Tiket/Laporan',
            'detail' => ''
        ],
        'deskripsi' => [
            'title' => 'Deskripsi Tiket/Laporan',
            'detail' => ''
        ],
        'kategori' => [
            'title' => 'Kategori Tiket/Laporan',
            'detail' => '',
            'select' => 'Pilih Kategori Laporan',
            'id' => 'ID Kategori',
            'nama' => [
                'title' => 'Nama Kategori',
                'detail' => 'Masukkan Nama Kategori'
            ],
            'deskripsi' => [
                'title' => 'Deskripsi Kategori',
                'detail' => 'Masukkan detail deskripsi'
            ]
        ],
        'unit_kerja' => [
            'title' => 'Unit Kerja / Fakultas',
            'detail' => '',
            'select' => 'Pilih Unit Kerja / Fakultas'
        ],
        'user_id' => [
            'title' => 'User ID',
            'detail' => ''
        ],
        'user' => [
            'title' => 'User',
            'detail' => ''
        ],
        'prioritas' => [
            'title' => 'Prioritas',
            'detail' => '',
        ],
        'status' => [
            'title' => 'Status',
            'detail' => '',
        ],
        'created_at' => [
            'title' => 'Tanggal Pembuatan',
            'detail' => ''
        ],
        'updated_at' => [
            'title' => 'Tanggal Perubahan',
            'detail' => ''
        ],
        'teknisi' => [
            'title' => 'Teknisi',
            'detail' => ''
        ],
        'vote' => [
            'title' => 'Vote',
            'detail' => ''
        ],
        'faq' => [
            'title' => 'FAQ',
            'detail' => ''
        ],

        'kesimpulan' => [
            'title' => 'Kesimpulan',
            'analisis' => [
                'title' => 'Analisis',
                'detail' => 'Analisis laporan/keluhan'
            ],
            'solusi' => [
                'title' => 'Solusi',
                'detail' => 'Solusi laporan/keluhan'
            ]
        ],

        'info' => [
            'status' => [
                'title' => 'Status Info',
                'detail' => 'Masukkan detail info'
            ],
            'deskripsi' => [
                'title' => 'Deskripsi Info',
                'detail' => 'Masukkan detail deskripsi info'
            ]
        ]
    ],

    'title' => [
        'name' => 'Nama',
    ],

    'detail' => [
        'name' => 'Masukan Nama',
    ]
];