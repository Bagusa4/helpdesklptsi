/**
 * Created by Bagusa4 on 2/8/2017.
 */

const state = {
    dataUrl: String,
    updateCount: 0,
    response: [],
    keys: []
}

const mutations = {
    setUrl (state, data) {
        state.dataUrl = data
    },
    updateCount (state) {
        state.updateCount++
    },
    updateResponse (state, data) {
        state.response = data
    },
    updateKeys (state, data) {
        state.keys = data
    }
}

const actions  = {
    getDataTable ({ dispatch, commit, state }, dataUrl) {
        if (state.getUrl == null) {
            commit('setUrl', dataUrl)
        }

        Vue.http.get(state.dataUrl).then(function(response) {
            commit('updateResponse', response.data)
            dispatch('filterKeys', { response: response.body, depth: 0})
        }, function(error) {
            Console.log(error)
        })

        commit('updateCount')
    },
    filterKeys ({ commit }, data) {
        var keys = [];
        var tmp_keys = [];

        _.forEach(data.response, function(value, key) {
            keys.push(_.keys(value))
        });
        _.forEach(keys[data.depth], function (value, key) {
            if(_.includes(value, '_')) {
                tmp_keys.push(_.capitalize(_.replace(value, '_', ' ')))
            } else {
                tmp_keys.push(_.capitalize(value))
            }
            keys = tmp_keys
        });

        commit('updateKeys', keys)

        return keys;
    },
    updateData ({ dispatch, commit, state }) {
        dispatch('getDataTable', state.dataUrl)
        commit('updateCount')
    }
}

const getters = {

}

export const dataTable = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}