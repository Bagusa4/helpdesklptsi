<?php

namespace App\Models;

use App\Models\Traits\LocalQueryScopeTrait;
use Illuminate\Database\Eloquent\Model;

class KategoriTiket extends Model
{
    use LocalQueryScopeTrait;

    /**
     * The attribute that for select spesific Table
     *
     * @var string
     */
    protected $table = 'KategoriTiket';

    /**
     * The attribute that for disable auto timestamp when insert new data
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Override this method to assign spesific column for ordering
     *
     * @return string
     */
    protected function orderingColumn()
    {
        return 'nama_kategori';
    }

    /**
     * Override this method to assign specific columns for references result
     *
     * @return array
     */
    protected function searchColumns()
    {
        return ["nama_kategori", "deskripsi_kategori"];
    }

    /**
     * belongsToMany Tiket
     * 'App\Models\Tiket'
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tiket()
    {
        return $this->belongsToMany(Tiket::class, 'KategoriTiket_Tiket');
    }
}
