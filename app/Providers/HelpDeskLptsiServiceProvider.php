<?php

namespace App\Providers;

use App\Http\ViewComposers\MenuViewComposer;
use App\Http\ViewComposers\QuotesViewComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class HelpDeskLptsiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.app', MenuViewComposer::class);
        view()->composer('*', QuotesViewComposer::class);

        Validator::extend('nim_username', function ($attribute, $value) {
            return $value;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
