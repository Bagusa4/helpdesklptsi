<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Collections\Menu;
use App\Models\KategoriTiket;
use App\Models\Tiket;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home')->with('data', ['menu' => Menu::getMenu()->all(), 'statistik' => $this->statistik()])->render();
    }

    private function statistik()
    {
        $statistik = collect();

        $statistik->push([
            'nama' => Lang::get('alias.statistik.manage.all_user'),
            'jumlah' => User::all()->count()
        ]);

        if (Auth::user()->hasRole(['Admin', 'Operator'])) {
            $statistik->push([
                'nama' => Lang::get('alias.statistik.manage.user'),
                'jumlah' => User::withRole('User')->get()->count()
            ]);
            $statistik->push([
                'nama' => Lang::get('alias.statistik.manage.teknisi'),
                'jumlah' => User::withRole('Teknisi')->get()->count()
            ]);
            $statistik->push([
                'nama' => Lang::get('alias.statistik.manage.operator'),
                'jumlah' => User::withRole('Operator')->get()->count()
            ]);
            $statistik->push([
                'nama' => Lang::get('alias.statistik.manage.admin'),
                'jumlah' => User::withRole('Admin')->get()->count()
            ]);
            $statistik->push([
                'nama' => Lang::get('alias.statistik.manage.confirmation'),
                'jumlah' => User::notActive()->get()->count()
            ]);
            $statistik->push([
                'nama' => Lang::get('alias.statistik.manage.trashed'),
                'jumlah' => User::onlyTrashed()->get()->count()
            ]);
        }

        $statistik->push([
            'nama' => Lang::get('alias.statistik.manage.unit_kerja'),
            'jumlah' => UnitKerja::all()->count()
        ]);

        $statistik->push([
            'nama' => Lang::get('alias.statistik.tiket.kategori'),
            'jumlah' => KategoriTiket::all()->count()
        ]);

        $statistik->push([
            'nama' => Lang::get('alias.statistik.tiket.tiket'),
            'jumlah' => Tiket::all()->count()
        ]);

        $statistik->push([
            'nama' => Lang::get('alias.statistik.tiket.ku'),
            'jumlah' => Tiket::withUser(Auth::user()->id)->withoutGlobalScopes()->count()
        ]);

        $statistik->push([
            'nama' => Lang::get('alias.statistik.tiket.faq'),
            'jumlah' => Tiket::faq()->get()->count()
        ]);

        return $statistik;
    }
}
