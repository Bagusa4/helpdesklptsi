<?php

namespace App\Http\Controllers\Teknisi;

use App\Http\Controllers\Traits\TiketInfoControllerTrait;
use App\Models\Tiket;
use App\Models\TiketInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class TiketInfoController extends Controller
{
    use TiketInfoControllerTrait;

    /**
     * TiketInfoController constructor.
     */
    public function __construct()
    {
        $this->middleware('role:Teknisi');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TiketInfo  $tiketInfo
     * @return \Illuminate\Http\Response
     */
    public function show(TiketInfo $tiketInfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TiketInfo  $tiketInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(TiketInfo $tiketInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tiket  $tiket
     * @param  \App\Models\TiketInfo  $info
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tiket $tiket, TiketInfo $info)
    {
        $this->validation($request->all())->validate();

        $tiketInfo = $info;

        $updateInfo = [
            'status_info' => $request->input('set.status_info'),
            'deskripsi_info' => $request->input('set.deskripsi_info')
        ];

        if (Auth::user()->id == $tiketInfo->user_id) {
            $tiketInfo->update($updateInfo);

            $update = $tiketInfo->save();
        } else {
            $update = false;
        }

        return response($this->notify($update, [
            Lang::get('messages.success.tiket.update.info.title'),
            Lang::get('messages.failed.tiket.update.info.title'),
            $tiketInfo
        ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tiket @tiket
     * @param  \App\Models\TiketInfo  $info
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tiket $tiket, TiketInfo $info)
    {
        $tiketInfo = $info;

        if (Auth::user()->id == $tiketInfo->user_id) {
            $delete = $tiketInfo->delete();
        } else {
            $delete = false;
        }

        return response($this->notify($delete, [
            Lang::get('messages.success.tiket.delete.info.title'),
            Lang::get('messages.failed.tiket.delete.info.title'),
            $tiketInfo
        ]));
    }
}
