<?php

namespace App\Http\Controllers;

class SuccessController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Success register
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register()
    {
        return view('success.register');
    }
}
