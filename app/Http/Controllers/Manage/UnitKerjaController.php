<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Traits\NotifyControllerTrait;
use App\Models\UnitKerja;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class UnitKerjaController extends Controller
{
    use NotifyControllerTrait;

    /**
     * UnitKerjaController constructor.
     */
    public function __construct()
    {
        $this->middleware('role:Admin|Operator');
    }

    public function data(Request $request, UnitKerja $unitKerja)
    {
        $search = $request->input('search');
        $searchId = $request->input('searchId');
        $orderBy = $request->input('orderBy');
        $customQuery = [
            'search' => $search,
            'searchId' => $searchId,
            'orderBy' => $orderBy
        ];

        $response = $unitKerja
            ->search($search)->findById($searchId)->orderingBy($orderBy)
            ->paginate(20);
        $response->appends($customQuery)->fragment('table')->links();

        return response()
            ->json($response)
            ->header('Content-Type', 'application/json');
    }

    protected function validation(Request $request)
    {
        return Validator::make($request->all(), [
            'nama' => 'nullable',
            'deskripsi' => 'nullable'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manage.unitkerja');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request)->validate();

        $unitKerja = new UnitKerja();
        $unitKerja->nama = $request->input('nama');
        $unitKerja->deskripsi = $request->input('deskripsi');

        $save = $unitKerja->save();

        return response()
            ->json($this->notify($save, [
                Lang::get('messages.success.unit_kerja.create.title'),
                Lang::get('messages.failed.unit_kerja.create.title'),
                $unitKerja
            ]))->header('Content-Type', 'application/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UnitKerja  $unitKerja
     * @return \Illuminate\Http\Response
     */
    public function show(UnitKerja $unitKerja)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UnitKerja  $unitKerja
     * @return \Illuminate\Http\Response
     */
    public function edit(UnitKerja $unitKerja)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UnitKerja  $unitKerja
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UnitKerja $unitKerja)
    {
        $this->validation($request)->validate();

        $unitKerja = $unitKerja->find($unitKerja->id);
        $unitKerja->nama = $request->input('nama');
        $unitKerja->deskripsi = $request->input('deskripsi');

        $update = $unitKerja->save();

        return response()
            ->json($this->notify($update, [
                Lang::get('messages.success.unit_kerja.update.title'),
                Lang::get('messages.failed.unit_kerja.update.title'),
                $unitKerja
            ]))->header('Content-Type', 'application/json');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UnitKerja  $unitKerja
     * @return \Illuminate\Http\Response
     */
    public function destroy(UnitKerja $unitKerja)
    {
        $destroy = $unitKerja->find($unitKerja->id)->delete();

        return response()
            ->json($this->notify($destroy, [
                Lang::get('messages.success.unit_kerja.delete.title'),
                Lang::get('messages.failed.unit_kerja.delete.title'),
                $unitKerja
            ]))->header('Content-Type', 'application/json');
    }
}
