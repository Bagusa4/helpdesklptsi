<?php

namespace App\Http\Controllers\Manage\Tiket;

use App\Http\Controllers\Traits\TiketControllerTrait;
use App\Models\Tiket;
use App\Models\TiketSolution;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TiketController extends Controller
{
    use TiketControllerTrait;

    /**
     * Do something when construtor called
     */
    public function __construct()
    {
        $this->middleware('role:Admin|Operator');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manage.tiket.tiket');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function show(Tiket $tiket)
    {
        return view('manage.tiket.show')->with('data', $tiket);
    }

    /**
     * Show result with pdf stream
     *
     * @param Tiket $tiket
     * @return string
     */
    public function pdf(Tiket $tiket)
    {
        $pdf = PDF::loadView('manage.tiket.pdf.detail', $this->getFind($tiket)->find($tiket->id));

        $pdf->setPaper('A4', 'potrait');

        $stream = $pdf->stream();
        //$stream = view('manage.tiket.pdf', $this->getFind($tiket)->find($tiket->id));

        return $stream;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function edit(Tiket $tiket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tiket $tiket)
    {
        $tiket->nama_pelapor = $request->input('set.nama_pelapor');
        $tiket->prioritas_tiket = $request->input('set.prioritas_tiket');
        $tiket->status_tiket = $request->input('set.status_tiket');
        $tiket->unit_kerja_id = $request->input('set.unit_kerja_id');

        if (count($kategori = $request->input('set.kategori')) !== 0) {
            $tiket->kategori()->detach();
            $tiket->kategori()->attach($kategori);
        }

        if (count($teknisi = $request->input('set.teknisi')) !== 0) {
            $tiket->teknisi()->detach();
            $tiket->teknisi()->attach($teknisi);
        }

        $updateSolution = [
            'tiket_id' => $tiket->id,
            'analisis' => $request->input('set.solution.analisis'),
            'solusi' => $request->input('set.solution.solusi'),
        ];
        if ($tiket->solution !== null) {
            $tiket->solution()->update($updateSolution);
        } else {
            $solution = TiketSolution::firstOrCreate($updateSolution);
            $solution->tiket()->associate($tiket);
        }

        $tiket->faq = $request->input('set.faq');

        $update = $tiket->save();

        return response()
                ->json($this->notify(
                    $update,
                    ['Berhasil di Update !!!', 'Gagal di update !!!', $tiket]
                ))
                ->header('Content-Type', 'application/json');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tiket $tiket)
    {
        $delete = $tiket->delete();

        return response()
                ->json($this->notify($delete, [
            'Berhasil di hapus',
            'Gagal di hapus',
            $tiket
        ]))->header('Content-Type', 'application/json');
    }
}
