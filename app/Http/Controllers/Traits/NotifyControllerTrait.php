<?php

namespace App\Http\Controllers\Traits;

trait NotifyControllerTrait
{
    /**
     * Send Response Notify
     *
     * @param $status
     * @param $data
     * @return array
     */
    protected function notify($status, $data)
    {
        $statuss = array();

        if(is_null($status) || ($status == true)) {
            $statuss = array('status' => true, 'message' => $data[0], 'response' => $data);
        } else {
            $statuss = array('status' => false, 'message' => $data[1], 'response' => $data);
        }

        return $statuss;
    }
}
