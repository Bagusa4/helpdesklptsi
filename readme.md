<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Keyakinan

Sebetulnya saya tidak yakin aplikasi ini akan dikembangkan ulang, karena mendengar workshop waktu itu, penyuluhan DevOPS.
Karena disitu aplikasi di kembangin bersama, melalui satu repository di gitlab, nah sedangkan ini saya kembangin seorang diri,
mungkin logikanya akan sulit di pahami, karena saya masih belajar, bisa di hitung banyak semrawut, belum ketemu jalan lurus.
Tapi ya sudah, toh pembuatan aplikasi ini bagi saya untuk pembelajaran dan tentu untuk memenuhi kegiatan PKL :D 

## Dari Mana ?
Semua sumber pembelajaran saya ada di internet, pertanyaan keluhan dan bug yang ada saya coba cari di internet, juga ada
yang di solve sendiri, maupun mengubah solusi dari internet,.
Dokumentasi pun saya langsung dari vendor yang menyediakan.
Juga hampir keseluruhan kode saya sudah saya berikan dokumentasi, jadi mudah-mudahan bisa dipahami :) , kalo salah mohon
dimaklumi :)

## Ada Apa AJA ?
- Buat Backend saya pake Laravel
- Buat Database bisa mana saja, karena saya gunakan ORM dari laravel, jarang pke query manual
- Buat Frontend saya pakai framework VueJS :) , kebetulan katanya gampang dan fleksibel, ternyata iya
  buat nanti sekalian belajar frontend yg lebih kyk ReactJS atau AngularJS
- Dan mungkin itu saja :)

## Resource / Data Default 
- Menggunakan Custom Command ARTISAN, saya membuat command untuk build
    data, dari default data hingga hak akses, itu salah satu teknik, mungkin ada
    teknik lain ? , mohon sarannya :)
- Untuk Data Default yang saya maksud, semua berada pada folder "database".
    - factories 
    - seeds
- Untuk folder migrations, untuk migrasi blueprint relasi entitas :)

## Link Saya
- ID : Bagusa4 ( Hampir Semua SOSMED inisial saya itu :) ) . . .
- Cheers !!!

## !_! ##

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
